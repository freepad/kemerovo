const Fastify = require('fastify')
const fastifyIO = require("fastify-socket.io");
const cors = require('@fastify/cors')

const app = Fastify({
    logger: process.env.NODE_ENV === 'development',
})
app.register(fastifyIO);
app.register(cors);

let id = 0;
const teams = new Set();

app.get('/', (req, reply) => {
  reply.send({ status: 'it works' })
})
app.post('/add', (req, reply) => {
    // @ts-expect-error FIXME
    const teamName = req.body.teamName
    if (teams.has(teamName)) {
        reply.send({
            success: false,
            error: {
                message: 'Команда с таким названием уже существует'
            }
        })
        return
    } else {
        teams.add(teamName)
        reply.send({
            success: true
        })
    }
})

app.post('/has', (req, reply) => {
    // @ts-expect-error FIXME
    const teamName = req.body.teamName
    if (teams.has(teamName)) {
        reply.send({
            success: true,
        })
        return
    } else {
        reply.send({
            success: false,
        })
        return
    }
})

app.post('/team/remove', (req, reply) => {
    // @ts-expect-error FIXME
    const teamName = req.body.teamName
    if (!teams.has(teamName)) {
        reply.send({
            success: false,
            error: {
                message: 'Команда с таким названием не существует'
            }
        })
        return
    } else {
        teams.delete(teamName)
        reply.send({
            success: true,
            teams: Array.from(teams)
        })
    }
})

app.get('/teams', (req, reply) => {
    reply.send({
        success: true,
        teams: Array.from(teams)
    })
})

const answers = new Map();
app.post('/answer', (req, reply) => {
    // @ts-expect-error FIXME
    const teamName = req.body.teamName
    if (!teams.has(teamName)) {
        reply.send({
            success: false,
            error: {
                message: 'Команда с таким названием не существует'
            }
        })
        return
    } else if (answers.has(teamName)) {
        reply.send({
            success: false,
            error: {
                message: 'Вы уже отвечали. Дождитесь следующего вопроса'
            }
        })
        return
    } else {
        answers.set(teamName, Date.now())
        reply.send({
            success: true
        })
    }
})

app.get('/answers', (req, reply) => {
    reply.send({
        success: true,
        answers: Array.from(answers.entries())
            .sort((a, b) => b[1] - a[1])
            .map(([name, time]) => ({ name, time }))
    })
})

app.post('/reset', (req, reply) => {
    answers.clear();
    reply.send({
        success: true
    })
})

module.exports = {
    default: app
}
