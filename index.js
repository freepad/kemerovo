const express = require("express");
const app = express();
const port = process.env.PORT || 3001;
const cors = require('cors')
const bodyParser = require('body-parser')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(cors())

app.get("/", (req, res) => res.type('html').send('<h1>Hello Kemerovo!</h1>'));

const teams = new Set();

app.get('/', (req, reply) => {
    reply.send({ status: 'it works' })
})
app.post('/add', (req, reply) => {
    // @ts-expect-error FIXME
    const teamName = req.body.teamName
    if (teams.has(teamName)) {
        reply.json({
            success: false,
            error: {
                message: 'Команда с таким названием уже существует'
            }
        })
        return
    } else {
        teams.add(teamName)
        reply.json({
            success: true
        })
    }
})

app.post('/has', (req, reply) => {
    // @ts-expect-error FIXME
    const teamName = req.body.teamName
    if (teams.has(teamName)) {
        reply.json({
            success: true,
        })
        return
    } else {
        reply.json({
            success: false,
        })
        return
    }
})

app.post('/team/remove', (req, reply) => {
    // @ts-expect-error FIXME
    const teamName = req.body.teamName
    if (!teams.has(teamName)) {
        reply.json({
            success: false,
            error: {
                message: 'Команда с таким названием не существует'
            }
        })
        return
    } else {
        teams.delete(teamName)
        reply.json({
            success: true,
            teams: Array.from(teams)
        })
    }
})

app.get('/teams', (req, reply) => {
    reply.json({
        success: true,
        teams: Array.from(teams)
    })
})

const answers = new Map();
app.post('/answer', (req, reply) => {
    // @ts-expect-error FIXME
    const teamName = req.body.teamName
    if (!teams.has(teamName)) {
        reply.json({
            success: false,
            error: {
                message: 'Команда с таким названием не существует'
            }
        })
        return
    } else if (answers.has(teamName)) {
        reply.json({
            success: false,
            error: {
                message: 'Вы уже отвечали. Дождитесь следующего вопроса'
            }
        })
        return
    } else {
        answers.set(teamName, Date.now())
        reply.json({
            success: true
        })
    }
})

app.get('/answers', (req, reply) => {
    reply.json({
        success: true,
        answers: Array.from(answers.entries())
          .sort((a, b) => b[1] - a[1])
          .map(([name, time]) => ({ name, time }))
    })
})

app.post('/reset', (req, reply) => {
    answers.clear();
    reply.json({
        success: true
    })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
